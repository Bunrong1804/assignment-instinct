import 'package:flutter/material.dart';

class MyLogo extends StatefulWidget {
  @override
  _MyLogoState createState() => _MyLogoState();
}
class _MyLogoState extends State<MyLogo> {
  double width;
  get _builCicle {
    return  Container(
      // margin: EdgeInsets.symmetric(horizontal: 0,vertical: 10),
      height: 40,
      width: 40,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        color: Colors.grey[300],

      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            child: Text('Facebook' , style: TextStyle(color: Colors.blue , fontSize: 26 , fontWeight: FontWeight.bold,),),
          ),
          SizedBox(width: width/2.5,),
          Stack(
           children: [
             _builCicle,
             Positioned(
               top: -2,
                 right: 3,
                 child: Container(
                   // color: Colors.yellow,
                   height: 40,
                   width: 40,
                   child: IconButton(icon: Icon(Icons.search , color: Colors.black,),iconSize: 30,),)),
           ],
          ),
          Stack(
           children: [
             _builCicle,
             Positioned(
               top: -2,
                 right: 3,
                 child: Container(
                   // color: Colors.yellow,
                   height: 40,
                   width: 40,
                   child: IconButton(icon: Icon(Icons.message , color: Colors.black,),iconSize: 30,color: Colors.black,),)),
           ],
          ),
        ],
      ),
    );
  }
}
