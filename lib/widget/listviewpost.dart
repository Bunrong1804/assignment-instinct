import 'package:flutter/material.dart';

import '../item.dart';
import 'package:page_transition/page_transition.dart';

class listViewPost extends StatefulWidget {
  @override
  _listViewPostState createState() => _listViewPostState();
}
class _listViewPostState extends State<listViewPost> {
  double _weigth;
  @override
  Widget build(BuildContext context) {
    _weigth = MediaQuery.of(context).size.width;
    return Container(
      child: _buildListView,
    );
  }
  get _buildListView{
    return Container(
      height: 300, width: _weigth,
      child: ListView.builder(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.vertical, itemCount: itemList.length,
        itemBuilder: (context , index){
          return _buildItemList(itemList[index]);
        },
      ),
    );
  }
  _buildItemList(Item item){
    return InkWell(
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
              width: _weigth, margin: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                image: DecorationImage(
                  image: NetworkImage(item.img ), fit: BoxFit.cover,),
              )
          ),

          InkWell(
            child: Container(
              height: 80, width: 80,
              child: Icon(Icons.play_circle_fill_rounded , color: Colors.white60, size: 80,),
            ),
          ),
          Positioned(
              top: 20,
              child: Container(
                height: 30, width: 150, alignment: Alignment.center, padding: EdgeInsets.symmetric(vertical: 5 , horizontal: 5), color: Colors.white60,
                child: Text(item.title , overflow: TextOverflow.ellipsis,),
              )

          ),
        ],

      ),
    );

  }
}
