import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../item.dart';
import 'package:page_transition/page_transition.dart';

class suggestFriend extends StatefulWidget {
  @override
  _suggestFriendState createState() => _suggestFriendState();
}

class _suggestFriendState extends State<suggestFriend> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      child: _buildListView,
    );
  }
  get _buildListView{
    return Container(
      margin: EdgeInsets.only(top: 10), color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.only(left: 20), alignment: Alignment.topLeft,
                child: Text('People You May Know',style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
              ),
              IconButton(icon: Icon(Icons.more_horiz), onPressed: null),
            ],
          ),
          Container(
            height: 500, color: Colors.white,
            child: ListView.builder(
              physics: BouncingScrollPhysics(), scrollDirection: Axis.horizontal, itemCount: itemList.length,
              itemBuilder: (context , index){
                return _buildItemList(itemList[index]);
              },
            ),
          ),
          Container(
            height: 50, color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('See All'),
                IconButton(icon: Icon(Icons.navigate_next), onPressed: null)
              ],
            ),
            
          ),
        ],
      ),
    );
  }
  _buildItemList(Item item){
    return InkWell(
      onTap: (){
      },
      child: Card(
        child: Column(
          children: [
            Container(
                height: 350,
                width: 300,
                margin: EdgeInsets.symmetric(horizontal: 10,vertical: 0),
                decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(20) , topRight: Radius.circular(20)),
                  image: DecorationImage(
                    image: NetworkImage(item.img ), fit: BoxFit.cover,),
                )
            ),
            Container(
              color: Colors.white, height: 130, width: 300,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 20), alignment: Alignment.topLeft,
                    child: Text('${item.title}',overflow: TextOverflow.ellipsis,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold ,) ,),
                  ),
                  SizedBox(height: 30,),
                  Container(

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                      child: Container(
                        height: 45, width: 150,
                        decoration: BoxDecoration(
                          color: Colors.blue, borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          children: [
                            IconButton(icon: Icon(Icons.person_add_alt_1 , color: Colors.white,), onPressed: null),
                            Text('Add Friend', style: TextStyle(color: Colors.white),)
                          ],
                        ),
                      ),
                    ),
                        Container(
                      child: Container(
                        height: 45, width: 100, decoration: BoxDecoration(color: Colors.grey[300],
                          borderRadius: BorderRadius.circular(10),
                        ),

                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text('Remove', style: TextStyle(color: Colors.black),)
                          ],
                        ),
                      ),
                    ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      )

    );

  }
}
