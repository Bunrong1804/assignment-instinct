import 'package:flutter/material.dart';

class NewFeedProfile extends StatefulWidget {
  @override
  _NewFeedProfileState createState() => _NewFeedProfileState();
}
class _NewFeedProfileState extends State<NewFeedProfile> {
  double h1 ;
  double _weigth ;
  @override
  Widget build(BuildContext context) {
    h1 = 16;
    _weigth = MediaQuery.of(context).size.width;

    return Container(
      color: Colors.white,
      // height: 700,
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.grey[300],

                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100.0),
                  child: Image.network('https://scontent.fpnh3-1.fna.fbcdn.net/v/t1.0-9/91202814_325543975073351_3888140705163378688_o.jpg?_nc_cat=104&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGeJrYwOp1UhVJiS-ruWwyDHQa62flUbBMdBrrZ-VRsE14NJcGb0y60mIU3A4KERo_TTZVYGw731Ge_H3o_LtjZ&_nc_ohc=FhAuLfptW7oAX9RPn0-&_nc_ht=scontent.fpnh3-1.fna&oh=d4be31dc0dcf0db366d5ac0304aa0b22&oe=602590FE' , fit: BoxFit.cover,),
                ),
              ),
              Container(
                  // width: 10,
                  child: Text('What\'s on your mine?' , style: TextStyle(fontSize: h1,),),
              ),
            ],
          ),

         Container(
           width: _weigth,
           margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
           padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
           decoration: BoxDecoration(
             color: Colors.white,
               border: Border.all(color: Colors.black12),
           ),
           child: Row(
             mainAxisAlignment: MainAxisAlignment.spaceAround,
             children: [
               Container(
                 child: Row(
                   children: [
                     Container(child: Icon(Icons.videocam, color: Colors.red,),),
                     Container(child: Text('Video'),),
                   ],
                 ),
               ),
               Container(
                 child: Row(
                   children: [
                     Container(
                       child: Icon(Icons.photo_library, color: Colors.green,),),
                     Container(child: Text('Video'),),
                   ],
                 ),
               ),
               Container(
                 child: Row(
                   children: [
                     Container(child: Icon(Icons.video_call, color: Colors.purple,),),
                     Container(child: Text('Room'),),
                   ],
                 ),
               ),
             ],
           ),
         ),
        ],
      ),
    );
  }
}
