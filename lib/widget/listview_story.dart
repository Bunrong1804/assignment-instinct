import 'package:flutter/material.dart';

import '../item.dart';
import 'package:page_transition/page_transition.dart';

class listViewStory extends StatefulWidget {
  @override
  _listViewStoryState createState() => _listViewStoryState();
}

class _listViewStoryState extends State<listViewStory> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: _buildListView,
    );
  }

  get _buildListView {
    return Container(
      height: 300,
      child: ListView.builder(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: itemList.length,
        itemBuilder: (context, index) {
          return _buildItemList(itemList[index]);
        },
      ),
    );
  }
  _buildItemList(Item item) {
    return InkWell(
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
              height: 300,
              width: 125,
              margin: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                image: DecorationImage(
                  image: NetworkImage(item.img),
                  fit: BoxFit.cover,
                ),
              )),
          Positioned(
              bottom: 20,
              child: Container(
                height: 30,
                width: 150,
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                child: Text(
                  item.title,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              )),
          Positioned(
            top: 8,
            left: 8,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                border: Border.all(
                  color: Colors.blue,
                  style: BorderStyle.solid,
                  width: 2.0,
                ),
                color: Colors.blue,
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100.0),
                child: Image.network(
                  item.img,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
