class Item {
  String title, img, body;

  Item({this.title, this.img, this.body});
}

List<Item> itemList = [
  Item(
    title: "Ton ChanSeyma តន់ ចន្ទសីម៉ា",
    body:
        "The saga of the Eternals, a race of immortal beings who lived on Earth and shaped its history and civilisations.",
    img:
        "https://scontent.fpnh20-1.fna.fbcdn.net/v/t1.0-9/121809184_2795557647379047_8967199595872395749_n.jpg?_nc_cat=1&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeFajPBjPf-cuxCYYW95E4sn9y3MUyc5xMX3LcxTJznExbm9STC9Igu2j2bFv0sIkCIsWlCURZUzdGFkYc40JERA&_nc_ohc=yO49eb4oSp4AX-fX4S1&_nc_ht=scontent.fpnh20-1.fna&oh=12aa50dff75945479d2961d86858c9da&oe=6030E188",
  ),
  Item(
    title: "Ban MonyLeak",
    body:
    "Shang-Chi is a master of numerous unarmed and weaponry-based wushu styles, including the use of the gun, nunchaku, and jian.",
    img:
    "https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-9/121485937_1299705063717473_4189265909164425201_n.jpg?_nc_cat=111&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGk5aSyT1CVs3qUb6zOGnerkgbply3GgGqSBumXLcaAaoXbtiOU0Sz2OMKofQ61kOVojoO6RMjRcvWJqrPaEhSQ&_nc_ohc=fWNbNd4_YmAAX_sBJZH&_nc_ht=scontent-sin6-1.xx&oh=0a7d46643f5c1a4a6dbfb9d8fb7216e8&oe=602E10E7",
  ),
  Item(
    title: "អ៊ុក សុវណ្ណារី",
    body:
    "At birth the Black Widow (aka Natasha Romanova) is given to the KGB, which grooms her to become its ultimate operative. When the U.S.S.R. breaks up, the government tries to kill her as the action moves to present-day New York, where she is a freelance operative.",
    img:
    "https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-9/83698883_104999431068362_4404119294212833280_o.jpg?_nc_cat=102&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeEqH2JZmkvb2xmHB5dz-I_dK0u-4wOJoAQrS77jA4mgBCTMELV9dVVijXZzenM1P5NT-M8xGH7MdlJgxPlcVCyz&_nc_ohc=0Ro0uu8zFFoAX_mf78j&_nc_ht=scontent-sin6-2.xx&oh=0e8f98efc25d4f04a2934201d1025a93&oe=603018A6",
  ),
  Item(
    title: "ខាន់ ជេមស៌ KHAN JAMES",
    body:
    "Biochemist Michael Morbius tries to cure himself of a rare blood disease, but when his experiment goes wrong, he inadvertently infects himself with a form of vampirism instead.",
    img:
    "https://scontent.fpnh20-1.fna.fbcdn.net/v/t1.0-9/131965543_3925444174161870_4977343789984846757_n.jpg?_nc_cat=1&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeEWI-vFa89j0PP3SA6FKKFu_Fp_nyaIQFz8Wn-fJohAXIjj86Znv7l3mQldUzvA15kFnsUkXSfJkwQZiXx32jFh&_nc_ohc=ZeaHy_HNtVgAX8BsfZt&_nc_ht=scontent.fpnh20-1.fna&oh=754af71298f361447bbe9b48bcf576bb&oe=602E91D9",
  ),

  Item(
    title: "Pich Thai",
    body:
    "Long ago, in the fantasy world of Kumandra, humans and dragons lived together in harmony. However, when sinister monsters known as the Druun threatened the land, the dragons sacrificed themselves to save humanity. Now, 500 years later, those same monsters have returned, and it's up to a lone warrior to track down the last dragon and stop the Druun for good.",
    img:
    "https://scontent-sin6-1.xx.fbcdn.net/v/t1.0-9/134034241_432371967916735_7300894275903832051_n.jpg?_nc_cat=100&ccb=2&_nc_sid=825194&_nc_eui2=AeHeV2fK-Uhb8QxWpiiu3Ck5bg5FD929vVZuDkUP3b29Vt7okywu-BP7dYLIYkccEGw7yrlDS21Kfte2YcaymENK&_nc_ohc=wIieNJzLdy0AX8sxbBd&_nc_ht=scontent-sin6-1.xx&oh=ab5214badd4b3dc96cac5ac12e311412&oe=60303D93",
  ),
  Item(
    title: "Chan Vathanaka",
    body:
    "When a single mother and her two children move to a new town, they soon discover that they have a connection to the original Ghostbusters and their grandfather's secret legacy.",
    img:
    "https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-9/61187673_2330707347206061_1904930803092029440_n.jpg?_nc_cat=105&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGFVJFo-NjxjmR2_7iwGYnZVBtfOiuYradUG186K5itp_ufQ58HMZmrFNpR_6bS6j8sRXsNl_zifJU-5sp6GwF0&_nc_ohc=LZMBATRAGk4AX9nDA0f&_nc_ht=scontent-sin6-2.xx&oh=f22eb3594265b6783b4f6fc5d4583c19&oe=602D6C5A",
  ),
  Item(
    title: "Bayyareth Nop",
    body:
    "Pete \"Maverick\" Mitchell keeps pushing the envelope after years of service as one of the Navy's top aviators. He must soon confront the past while training a new squad of graduates for a dangerous mission that demands the ultimate sacrifice.",
    img:
    "https://scontent-sin6-3.xx.fbcdn.net/v/t1.0-9/126942169_3810366682347843_1989113740068378144_n.jpg?_nc_cat=104&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeHen1dLrDC3Xpu1a8At3AM71PudLFiIt8_U-50sWIi3zxfkK5PYz_KmC0CbG-YrPLTY_-fEqsHp_FnIkQqUuU6g&_nc_ohc=x4-43L-0GtYAX-QhMKP&_nc_ht=scontent-sin6-3.xx&oh=d7cad9d3e9a1c639dbc8dc6860664386&oe=602F49F1",
  ),

];
