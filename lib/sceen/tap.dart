import 'package:assignmentinstinct/sceen/home.dart';
import 'package:assignmentinstinct/sceen/menu.dart';
import 'package:assignmentinstinct/sceen/profile.dart';
import 'package:flutter/material.dart';



class Tap extends StatefulWidget {
  @override
  _TapState createState() => _TapState();
}


class _TapState extends State<Tap> {

  int currentTap = 0;

  Widget buildTapBody(int current){
    switch (current) {
      case 0:
        return new Myhomepage();
        break;
      case 1:
        return new Myhomepage();
        break;
      case 2:
        return new Myhomepage();
        break;
        case 3:
        return new Myhomepage();
        break;
        case 4:
        return new Myhomepage();
        break;
        case 5:
        return new menuScreen();
        break;
      default:
        return menuScreen();
        break;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        return null;
      },
      child: Scaffold(
        body: buildTapBody(currentTap),
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.blueAccent,
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.white,
          currentIndex: currentTap,
          onTap: (val){
            setState(() {
              currentTap = val;
            });
          },
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home), label: ''),
            BottomNavigationBarItem(icon: Icon(Icons.ondemand_video), label: ''),
            BottomNavigationBarItem(icon: Icon(Icons.group_work), label: ''),
            BottomNavigationBarItem(icon: Icon(Icons.flag), label: ''),
            BottomNavigationBarItem(icon: Icon(Icons.notifications,), label: ''),
            BottomNavigationBarItem(icon: Icon(Icons.menu), label: ''),
          ],
        ),
      ),
    );
  }
}
