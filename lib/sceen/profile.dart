import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}
class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              title: Text('Profile'), pinned: true, expandedHeight: 340,
              flexibleSpace: FlexibleSpaceBar(
                background: Container(
                  decoration: BoxDecoration(
                    color: Colors.blue, borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30),
                    ),
                  ),
                  child: Column(children: [
                    SizedBox(height: 100,),
                    Center(
                      child: Container(
                        height: 170, width: 170,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100), border: Border.all(color: Colors.white, width: 3),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100.0),
                          child: Image.network('https://scontent.fpnh3-1.fna.fbcdn.net/v/t1.0-9/91202814_325543975073351_3888140705163378688_o.jpg?_nc_cat=104&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGeJrYwOp1UhVJiS-ruWwyDHQa62flUbBMdBrrZ-VRsE14NJcGb0y60mIU3A4KERo_TTZVYGw731Ge_H3o_LtjZ&_nc_ohc=FhAuLfptW7oAX9RPn0-&_nc_ht=scontent.fpnh3-1.fna&oh=d4be31dc0dcf0db366d5ac0304aa0b22&oe=602590FE'),
                        ),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      child: Text('BUNRONG VYROM', style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(child: Text('096 742 5680', style: TextStyle(color: Colors.white),),
                    ),
                  ]),
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                <Widget>[
                  Container(
                    color: Colors.white,
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 20, horizontal: 30),
                            child: Text('Account', style: TextStyle(fontSize: 20,),
                            ),
                          ),
                          Divider(
                            color: Colors.grey,
                          ),
                          Container(
                            child: Column(
                              children: [
                                ListTile(leading: IconButton(icon: Icon(Icons.bubble_chart),), title: Text('Change Password'),),
                                ListTile(leading: IconButton(icon: Icon(Icons.verified_user),), title: Text('Update Information'),),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30), child: Text('About Application', style: TextStyle(fontSize: 20,),
                            ),),
                          Divider(color: Colors.grey,),
                          Container(
                            child: Column(
                              children: [
                                ListTile(leading: IconButton(icon: Icon(Icons.settings),), title: Text('Setting'),),
                                ListTile(leading: IconButton(icon: Icon(Icons.privacy_tip),), title: Text('Account Privacy'),),
                                ListTile(leading: IconButton(icon: Icon(Icons.model_training_outlined),), title: Text('Dark Mode'),),
                              ],),),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
