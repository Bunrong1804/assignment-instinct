import 'package:assignmentinstinct/widget/facebook_logo.dart';
import 'package:assignmentinstinct/widget/listview_story.dart';
import 'package:assignmentinstinct/widget/newfeed_profile.dart';
import 'package:assignmentinstinct/widget/post_profile.dart';
import 'package:assignmentinstinct/widget/suggest_friends.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:page_transition/page_transition.dart';


import '../item.dart';
import '../item.dart';
import '../item.dart';
import '../itempost.dart';

class Myhomepage extends StatefulWidget {
  @override
  _MyhomepageState createState() => _MyhomepageState();
}

class _MyhomepageState extends State<Myhomepage> {
  double  width;
  @override
  Widget build(BuildContext context) {
  width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 27 ,left: 0 ,right: 0),
          color: Colors.grey,
          child: Column(
            children: [
              Container(

                child: MyLogo(),
              ),
             Container(
               alignment: Alignment.topLeft,
               child:  NewFeedProfile(),
             ),
             Container(
               height: 250,
               width: width,
               color: Colors.white,

               margin: EdgeInsets.symmetric(horizontal: 0,vertical: 10),
               child: listViewStory(),


             ),
             Container(
               margin: EdgeInsets.symmetric(horizontal: 0,vertical: 0),
               child: _buildListView,

             ),
             Container(
               margin: EdgeInsets.symmetric(horizontal: 0,vertical: 0),
               child: suggestFriend(),

             ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 0,vertical: 7),
                child: _buildListView,

              ),


            ],
          ),
        ),
      ),
    );
  }

  get _buildListView{
    return Container(
      child: Column(
        children: List.generate(itemPostList.length, (index) {
          return _buildItemList(itemPostList[index]);
        }),
      )
    );
  }

  _buildItemList(ItemPost item){
    return InkWell(
      onTap: (){
      },

      child: Card(
        child: Container(
          padding: EdgeInsets.only(top: 10),
          width: width,
          child: Column(
            children: [
              Container(
                child:  Container(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 10,vertical: 0),
                            height: 35,
                            width: 35,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              color: Colors.grey[300],

                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(100.0),
                              child: Image.network(item.profile , fit: BoxFit.fitWidth,),
                            ),


                          ),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(item.user_names , style: TextStyle(fontSize: 16,),),
                                Text('4 Oct 2020' , style: TextStyle(fontSize: 11,),),
                              ],
                            ),
                          ),

                        ],
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 10,right: 10 ,top: 5 , bottom: 2),
                        alignment: Alignment.topLeft,
                        child: Text(item.caption),
                      ),
                    ],
                  ),
                ),
              ),

              Container(

                child: Image.network(item.img , fit: BoxFit.contain,)
              ),
              Container(height: 20,),
              //number like
              Container(
                width: width, margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0), padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(child: Icon(FontAwesome.thumbs_up, color: Colors.blue, size: 16,),),
                          Container(child: Text('2'),)
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Container(
                            child: null,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Container(child: Text('17 Comments 5 Shares'),),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: width,
                margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0), padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5), color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Container(child: Icon(FontAwesome.thumbs_o_up, color: Colors.grey,size: 20,),),
                          Container(child: Text('Like'),)
                        ],
                      ),
                    ),
                    Container(

                      child: Row(
                        children: [
                          Container(
                            child: Icon(FontAwesome5.comment_alt, color: Colors.grey,size: 20,),),
                          Container(child: Text('Comment'),),
                        ],
                      ),
                    ),

                    Container(
                      child: Row(
                        children: [
                          Container(child: Icon(FontAwesome.share, color: Colors.grey,size: 20,),),
                          Container(child: Text('Share'),),
                        ],
                      ),
                    ),
                  ],
                ),

              ),

        

            

            ],
          ),
        ),
      ),
    );

  }
}
