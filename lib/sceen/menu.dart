import 'package:assignmentinstinct/sceen/profile.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class menuScreen extends StatefulWidget {
  @override
  _menuScreenState createState() => _menuScreenState();
}
class _menuScreenState extends State<menuScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20,vertical: 0),
            child:  Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(

                  alignment: Alignment.topLeft,
                  child: Text('Menu',style: TextStyle(fontSize: 28,fontWeight: FontWeight.bold),),
                ),
                IconButton(icon: Icon(Icons.search , size: 30,), onPressed: null),
              ],
            ),
          ),
          SizedBox(height: 20,),
          Container(
            alignment: Alignment.topLeft, height: 50,
            child: InkWell(
              onTap: (){
                Navigator.push(context,
                    PageTransition(
                      child: ProfileScreen(),
                    )
                );
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      border: Border.all(color: Colors.white , width: 3),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100.0),
//                     child: Image.asset('assets/img/banner/vyrom.jpg' , height: 50 , width: 50, fit: BoxFit.cover ,),
                      child: Image.network('https://scontent.fpnh3-1.fna.fbcdn.net/v/t1.0-9/91202814_325543975073351_3888140705163378688_o.jpg?_nc_cat=104&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGeJrYwOp1UhVJiS-ruWwyDHQa62flUbBMdBrrZ-VRsE14NJcGb0y60mIU3A4KERo_TTZVYGw731Ge_H3o_LtjZ&_nc_ohc=FhAuLfptW7oAX9RPn0-&_nc_ht=scontent.fpnh3-1.fna&oh=d4be31dc0dcf0db366d5ac0304aa0b22&oe=602590FE'),

                    ) ,
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    width: 320,
                            decoration: BoxDecoration(
                              border:Border(
                              // top: BorderSide(width: 16.0, color: Colors.lightBlue.shade600),
                    bottom: BorderSide(width: 0.6, color: Colors.black),
                    ),
                            ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('Bunrong Vyrom', style: TextStyle(fontWeight: FontWeight.bold , fontSize: 18),),
                        Text('See Your Profile' , style: TextStyle( fontSize: 13), textAlign: TextAlign.left,),
                      ],
                    ),
                  ),

                ],
              ),
            ),
          ),
          SizedBox(height: 20,),
          Container(
            alignment: Alignment.topLeft, height: 50,
            child: InkWell(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      border: Border.all(color: Colors.white , width: 3),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100.0),
                      child: Image.network('https://scontent.fpnh20-1.fna.fbcdn.net/v/t1.0-9/107511495_105851617867049_1249592090684928650_o.jpg?_nc_cat=107&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeFXbA-PbksP9p_pTNuFc3B8SOB2p5dye5BI4Hanl3J7kC7sFcPcfc-pBE-vTIVeSqCigDZ4dMalKFIufaqrUfmD&_nc_ohc=6hRsTap58U0AX8hGlk2&_nc_ht=scontent.fpnh20-1.fna&oh=c35700ec97104548b04857093936f0a7&oe=603052CE'),
                    ) ,
                  ),
                  Container(
                    alignment: Alignment.topLeft, width: 320,
                    decoration: BoxDecoration(
                            border:Border(
                    bottom: BorderSide(width: 0.6, color: Colors.black),
                    ),
                            ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('KH-CLOUD Solution', style: TextStyle(fontWeight: FontWeight.bold , fontSize: 18),),
                        Text('5 new' , style: TextStyle( fontSize: 13), textAlign: TextAlign.left,),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text('Setting'),
          ),
          ListTile(
            leading: Icon(Icons.logout),
            title: Text('Log Out'),
            onTap:(){


            },
          ),
        ],
      ),
    );
  }
}
